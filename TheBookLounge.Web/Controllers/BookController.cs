﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TheBookLounge.DataAccess;
using TheBookLounge.DataAccess.Models;
using TheBookLounge.Web.Helpers;

namespace TheBookLounge.Web.Controllers
{
    public class BookController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public BookController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<IActionResult> List(string searchString)
        {
            IEnumerable<Book> books = await _applicationDbContext.Books
                .Include(a => a.Author)
                .OrderBy(b => b.Title)
                .ToListAsync();

            if (!String.IsNullOrEmpty(searchString))
            {
                // Search by title, author and genre
                // The Search is case insensitive
                books = books.Where(s => s.Title.Contains(searchString, StringComparison.OrdinalIgnoreCase)
                                         || s.Author.AuthorName.Contains(searchString, StringComparison.OrdinalIgnoreCase)
                                         || s.Genre.Contains(searchString, StringComparison.OrdinalIgnoreCase));

                TempData["search"] = searchString;
            }

            return View(books);
        }

        // GET: BookController/Details/5
        public async Task<IActionResult> Details(int ? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            var book = await _applicationDbContext.Books
                .Include(c => c.Author)
                .FirstOrDefaultAsync(b => b.BookId == id);

            if (book == null)
            {
                return NotFound($"Book id {id} does not exist");
            }

            return View(book);
        }

        // GET: BookController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Book book, Author author)
        {
            var status = BookStatus.Creating;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var authors = await _applicationDbContext.Authors.ToListAsync();
            var sameAuthor = authors.FirstOrDefault(a => a.AuthorName == author.AuthorName);

            var newBook = new Book();

            if (sameAuthor != null)
            {

                newBook.Title = book.Title;
                newBook.ISBN = book.ISBN;
                newBook.Genre = book.Genre;
                newBook.IsAvailable = book.IsAvailable;
                newBook.AuthorId = sameAuthor.AuthorId;

            }
            else
            {

                newBook.Title = book.Title;
                newBook.ISBN = book.ISBN;
                newBook.Genre = book.Genre;
                newBook.IsAvailable = book.IsAvailable;
                newBook.Author = new Author {AuthorName = author.AuthorName};

            };

            await _applicationDbContext.Books.AddAsync(newBook);

            await UpdateCopies(newBook, status);

            await _applicationDbContext.SaveChangesAsync();

            TempData["message-create"] = "The book list has been updated successfully.";
            TempData["create"] = "Create";

            return RedirectToAction("Details", new { id = newBook.BookId });
                
        }

        // GET: HomeController1/Edit/5
        public async Task<ActionResult> Edit(int ? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            var book = await _applicationDbContext.Books
                .Include(c => c.Author)
                .FirstOrDefaultAsync(b => b.BookId == id);

            if (book == null)
            {
                return NotFound($"Book id {id} does not exist");
            }

            return View(book);
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Book book)
        {
            var status = BookStatus.Editing;

            if (book == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var bookToUpdate =  _applicationDbContext.Books
                .FirstOrDefault(b => b.BookId == book.BookId);

            if (bookToUpdate == null)
            {
                return NotFound($"Book id {book.BookId} does not exist");
            }

            bookToUpdate.Title = book.Title;
            bookToUpdate.ISBN = book.ISBN;
            bookToUpdate.Genre = book.Genre;
            bookToUpdate.IsAvailable = book.IsAvailable;

            _applicationDbContext.Books.Update(bookToUpdate);

            await UpdateCopies(bookToUpdate, status);

            await _applicationDbContext.SaveChangesAsync();

            TempData["message-edit"] = "The book has been modified.";
            TempData["edit"] = "Edit";

            return RedirectToAction("Details", new { id = book.BookId });
        }

        // GET: BookController/Delete/5
        [HttpGet]
        public async Task<ActionResult> Delete(int ? id)
        {
            var status = BookStatus.Deleting;

            if (id == null)
            {
                return BadRequest();
            }

            var selectedBook = await _applicationDbContext.Books
                .Include(c => c.Author)
                .FirstOrDefaultAsync(b => b.BookId == id);

            if (selectedBook == null)
            {
                return NotFound($"Book id {id} does not exist");
            }

            _applicationDbContext.Books.Remove(selectedBook);

            await UpdateCopies(selectedBook, status);

            await _applicationDbContext.SaveChangesAsync();

            TempData["message-delete"] = "The book has been successfully deleted from the system.";

            return RedirectToAction("List");
        }

        private async Task UpdateCopies(Book book, BookStatus status)
        {
            var sameBooks = await _applicationDbContext.Books
                .Where(b => b.Title.Contains(book.Title) && b.AuthorId == book.AuthorId)
                .Select(x => x).ToListAsync();

            switch (status)
            {
                case BookStatus.Creating:
                    sameBooks.Add(book);
                    await _applicationDbContext.SaveChangesAsync();
                    break;
                case BookStatus.Deleting:
                    sameBooks.Remove(book);
                    break;
                case BookStatus.Editing:
                    break;
            }

            var totalCopies = sameBooks.Count();
            var availableCopies = sameBooks.Count(b => b.IsAvailable == true);

            foreach (var item in sameBooks)
            {
                item.TotalCopies = totalCopies;
                item.AvailableCopies = availableCopies;
            }

            _applicationDbContext.Books.UpdateRange(sameBooks);
        }
    }
}
