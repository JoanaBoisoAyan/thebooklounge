﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheBookLounge.Web.Helpers
{
    public enum BookStatus
    {
        Creating,
        Deleting,
        Editing
    }
}
