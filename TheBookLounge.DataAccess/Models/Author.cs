﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TheBookLounge.DataAccess.Models
{
    public class Author
    {
        [ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorId { get; set; }

        [Required(ErrorMessage = "Please enter a name")]
        [StringLength(50)]
        [DisplayName("Author")]
        public string AuthorName { get; set; }

        public List<Book> Books { get; set; }
    }
}
