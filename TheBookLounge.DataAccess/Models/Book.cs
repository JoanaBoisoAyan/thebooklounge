﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Tracing;
using Microsoft.AspNetCore.Mvc;

namespace TheBookLounge.DataAccess.Models
{
    public class Book
    {
        [BindProperty]
        [ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookId { get; set; }

        [Required(ErrorMessage = "Please enter a title")]
        [StringLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// Basic format pre-checks ISBN
        /// Require 10 digits/Xs(no separators). e.g. [1888799972]
        /// Or:
        /// Require 3 separators out of 13 characters total. e.g. [1-888799-97-2]
        /// Or:
        /// Require 978/979 plus 10 digits (13 total). e.g. [9783161484100]
        /// Or:
        /// Require 4 separators out of 17 characters total. e.g. [978-3-16-148410-0]
        /// </summary>
        [Required(ErrorMessage = "Please enter an ISBN number")]
        [StringLength(17)]
        [RegularExpression(@"^(?:ISBN(?:-1[03])?:?●)?(?=[0-9X]{10}$|(?=(?:[0-9]+[-●]){3})[-●0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[-●]){4})[-●0-9]{17}$)(?:97[89][-●]?)?[0-9]{1,5}[-●]?[0-9]+[-●]?[0-9]+[-●]?[0-9X]$",
            ErrorMessage = "ISBN correct formats: 1888799972 / 1-888799-97-2 / 9783161484100 / 978-3-16-148410-0")]
                            
        public string ISBN { get; set; }

        [Required(ErrorMessage = "Please enter a genre")]
        [StringLength(25)]
        public string Genre { get; set; }

        [DisplayName("Available")]
        [DefaultValue(false)]
        public bool IsAvailable { get; set; }

        [BindProperty]
        [ScaffoldColumn(false)]
        [DisplayName("Total Copies")]
        public int TotalCopies { get; set; }

        [BindProperty]
        [ScaffoldColumn(false)]
        [DisplayName("Available Copies")]
        public int AvailableCopies { get; set; }

        [BindProperty]
        [ScaffoldColumn(false)]
        public int AuthorId { get; set; }

        public Author Author { get; set; }

    }
}