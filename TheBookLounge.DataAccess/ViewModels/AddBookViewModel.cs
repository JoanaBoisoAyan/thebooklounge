﻿using System;
using System.Collections.Generic;
using System.Text;
using TheBookLounge.DataAccess.Models;

namespace TheBookLounge.DataAccess.ViewModels
{
    public class AddBookViewModel
    {
        public Book Book { get; set; }
        public Author Author { get; set; }
    }
}
