﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheBookLounge.DataAccess.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "AuthorId", "AuthorName" },
                values: new object[,]
                {
                    { 1, "Shakespeare" },
                    { 2, "Unamuno" },
                    { 3, "Pier Lecarre" }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "BookId", "AuthorId", "AvailableCopies", "Genre", "ISBN", "IsAvailable", "Title", "TotalCopies" },
                values: new object[,]
                {
                    { 1, 1, 0, "Novel", "1111111111", false, "The first book", 2 },
                    { 4, 1, 0, "Novel", "978-3-16-148410-0", false, "The first book", 2 },
                    { 2, 2, 0, "History", "2-222222-22-2", false, "The second book", 1 },
                    { 3, 3, 0, "Drama", "9783161484100", false, "The third book", 4 },
                    { 5, 3, 0, "History", "1888799972", false, "The fifth book", 2 },
                    { 6, 3, 0, "History", "9793161484100", false, "The fifth book", 2 },
                    { 7, 3, 0, "Drama", "978-3-16-148410-0", false, "The seventh book", 4 },
                    { 8, 3, 0, "Drama", "978-3-16-148410-0", false, "The seventh book", 4 },
                    { 9, 3, 0, "Drama", "979-3-16-148410-0", false, "The seventh book", 4 },
                    { 10, 3, 0, "Drama", "979-3-16-148410-0", false, "The seventh book", 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Books",
                keyColumn: "BookId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "AuthorId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "AuthorId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "AuthorId",
                keyValue: 3);
        }
    }
}
